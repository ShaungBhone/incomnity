<main>
    <div class="grid grid-cols-3 gap-4">
        <section class="max-h-[26rem] sticky top-4 bg-white rounded-lg hover:ring-gray-300 hover:ring-1 duration-200 ease-in py-5 px-4">
            <div class="space-y-3">
                <h1 class="font-semibold uppercase text-sm">{{ __('Create a community') }}</h1>
                <div class="flex">
                    <svg class="h-4 w-4 text-gray-500 my-auto mr-2" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <p class="text-sm text-gray-500">{{ __('Ask what you want to know!') }}</p>
                </div>
                <form class="space-y-4" method="POST" action="#">
                    @csrf
                    <div>
                        <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')"
                            required autofocus placeholder="Name of your topic" />
                    </div>
                    <div>
                        <select
                            class="focus:border-blue-500 w-full focus:ring-blue-500 focus:ring-1 duration-150 ease-in focus:ring-opacity-50 rounded-md shadow-sm"
                            name="thread-category" id="thread-category">
                            <option>{{ __('Please Select') }}</option>
                            <option value="value-one">Thread Category one</option>
                            <option value="value-two">Thread Category two</option>
                            <option value="value-three">Thread Category three</option>
                        </select>
                    </div>
                    <div>
                        <textarea
                            class="form-textarea focus:border-blue-500 w-full focus:ring-blue-500 focus:ring-1 duration-150 ease-in focus:ring-opacity-50 rounded-md shadow-sm"
                            name="description" id="description" rows="5" placeholder="What's on your mind?"></textarea>
                    </div>
                    <div>
                        <div class="flex items-center justify-between space-x-3">
                            <x-jet-secondary-button class="flex">
                                <svg
                                    class="text-gray-600 w-4 transform -rotate-45" fill="none" viewBox="0 0 24 24"
                                    stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                                </svg>
                                <span class="ml-1">Attach</span>
                            </x-jet-secondary-button>
                            <x-jet-button>
                                Submit
                            </x-jet-button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <div class="col-span-2">
            <article>
                <nav class="flex items-center justify-between text-xs">
                    <ul class="flex uppercase font-semibold border-b-4 pb-3 space-x-10">
                        <li><a href="#" class="border-b-4 pb-3 border-blue-500">All Ideas (87)</a></li>
                        <li><a href="#"
                                class="text-gray-400 transition duration-150 ease-in border-b-4 pb-3 hover:border-blue-500">Considering
                                (6)</a></li>
                        <li><a href="#"
                                class="text-gray-400 transition duration-150 ease-in border-b-4 pb-3 hover:border-blue-500">In
                                Progress (1)</a></li>
                    </ul>

                    <ul class="flex uppercase font-semibold border-b-4 pb-3 space-x-10">
                        <li><a href="#"
                                class="text-gray-400 transition duration-150 ease-in border-b-4 pb-3 hover:border-blue-500">Implemented
                                (10)</a></li>
                        <li><a href="#"
                                class="text-gray-400 transition duration-150 ease-in border-b-4 pb-3 hover:border-blue-500">Closed
                                (55)</a></li>
                    </ul>
                </nav>
                <div class="mt-5">
                    <div class="flex space-x-4">
                        <x-jet-dropdown align="left" width="60">
                            <x-slot name="trigger">
                                <span class="inline-flex rounded-md">
                                    <button type="button"
                                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:bg-gray-50 hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition">
                                        Category name

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd"
                                                d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            </x-slot>

                            <x-slot name="content">
                                <div class="px-4 py-2 text-sm text-gray-400">
                                    {{ __('Manage Team') }}
                                </div>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                    {{ __('Profile') }}
                                </x-jet-dropdown-link>
                            </x-slot>
                        </x-jet-dropdown>

                        <x-jet-dropdown align="left" width="60">
                            <x-slot name="trigger">
                                <span class="inline-flex rounded-md">
                                    <button type="button"
                                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:bg-gray-50 hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition">
                                        Category name

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd"
                                                d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            </x-slot>

                            <x-slot name="content">
                                <div class="px-4 py-2 text-sm text-gray-400">
                                    {{ __('Manage Team') }}
                                </div>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                    {{ __('Profile') }}
                                </x-jet-dropdown-link>
                            </x-slot>
                        </x-jet-dropdown>
                    </div>
                </div> <!-- filters end -->
                <div class="mt-5 border p-4 rounded-lg bg-white hover:ring-1 hover:ring-gray-300 duration-150 ease-in">
                    <div class="flex justify-between">
                        <div class="inline-flex flex-1">
                            <div class="flex-none">
                                <img class="w-16 h-16 rounded ring-1 ring-blue-400"
                                    src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}">
                                <div class="mt-3 text-center text-xl">12k</div>
                                <div class="text-center text-md text-gray-500">Votes</div>
                            </div>
                            <div class="ml-5 space-y-3 w-full">
                                <div class="flex justify-between">
                                    <div class="flex space-x-2">
                                        <div><a href="#">Username</a></div>
                                        <div>&bullet;</div>
                                        <div class="text-gray-500 text-sm my-auto">coach</div>
                                        <div class="my-auto"><svg class="h-4 w-4" viewBox="0 0 20 20">
                                                <path fill-rule="evenodd"
                                                    d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                                    clip-rule="evenodd" />
                                            </svg></div>
                                    </div>
                                    <div class="bg-green-800 px-8 rounded-full mr-2">
                                        <span
                                            class="uppercase font-semibold text-white text-sm">{{ __('open') }}</span>
                                    </div>
                                </div>
                                <div>
                                    <a href="#" class="text-lg font-semibold hover:text-gr">Topic</a>
                                </div>
                                <div>
                                    <p class="text-justify text-sm line-clamp-3">
                                        Here's a block Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic
                                        obcaecati repellendus labore tempore eos aliquid ex voluptates, suscipit fugiat
                                        aut!
                                        Here's a block Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic
                                        obcaecati repellendus labore tempore eos aliquid ex voluptates, suscipit fugiat
                                        aut!
                                    </p>
                                </div>
                                <div class="flex justify-between">
                                    <div class="space-x-3 flex my-auto">
                                        <div class="text-sm text-gray-400">10 hours ago</div>
                                        <div>&bullet;</div>
                                        <div class="text-sm text-gray-400">category</div>
                                        <div>&bullet;</div>
                                        <div class="text-sm text-gray-400"><span>2</span> comments</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <x-jet-dropdown>
                            <x-slot name="trigger">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 hover:cursor-pointer"
                                    viewBox="0 0 20 20" fill="currentColor">
                                    <path
                                        d="M6 10a2 2 0 11-4 0 2 2 0 014 0zM12 10a2 2 0 11-4 0 2 2 0 014 0zM16 12a2 2 0 100-4 2 2 0 000 4z" />
                                </svg>
                            </x-slot>
                            <x-slot name="content">
                                <x-jet-dropdown-link href="#">
                                    {{ __('Mark as read') }}
                                </x-jet-dropdown-link>
                                <x-jet-dropdown-link href="#">
                                    {{ __('Delete') }}
                                </x-jet-dropdown-link>
                            </x-slot>
                        </x-jet-dropdown>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>

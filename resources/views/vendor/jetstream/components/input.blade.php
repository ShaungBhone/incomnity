@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'focus:border-blue-500 focus:ring-blue-500 focus:ring-1 duration-150 ease-in focus:ring-opacity-50 rounded-md shadow-sm']) !!}>

<?php

use Illuminate\Support\Facades\Route;

//TODO
// Route::redirect('/', 'welcome');

Route::get('/', function () {
    return view('welcome');
});

Route::view('/show', 'show');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
